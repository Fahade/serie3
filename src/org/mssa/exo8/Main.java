package org.mssa.exo8;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RegistreMarin rgm = new RegistreMarin();
		Marin m1 =  new Marin("Titouan","Lamazou",1955) ;
	    Marin m2 =  new Marin("Alain","Gautier",1962) ;
	    Marin m3 =  new Marin("Christophe","Auguin",1959) ;
	    Marin m4 =  new Marin("Armel","Le CL�ac'h",1977) ;

	    rgm.addMarin(m1);
	    rgm.addMarin(m2);
	    rgm.addMarin(m3);
	    rgm.addMarin(m4);
	    //System.out.println(rgm);
	    System.out.println("Nombre marins total dans le registre : " + rgm.countT());                                                      

	    Marin m5 =  new Marin("Michel","Desjoyeaux",1965) ;
	    Marin m6 =  new Marin("Vincent","Riou",1972) ;
	    Marin m7 =  new Marin("Fran�ois","Gabart",1983) ;
	    
	    rgm.addMarin(m5);
	    rgm.addMarin(m6);
	    rgm.addMarin(m7);
	    
	    //System.out.println(rgm.get(1970));                                                      
	    System.out.println("Nombre marins n�s en "+1950+" decennie : " + rgm.count(1950));                                                      
	    System.out.println("Nombre marins n�s en "+1960+" decennie : " + rgm.count(1960));                                                      
	    System.out.println("Nombre marins n�s en "+1970+" decennie : " + rgm.count(1970));                                                      
	    System.out.println("Nombre marins n�s en "+1980+" decennie : " + rgm.count(1980));                                                      

	    System.out.println("Nombre marins total dans le registre : " + rgm.countT());                                                      

	}

}
