package org.mssa.exo8;

public class Marin {
	private String nom;
	private String prenom;
	private int naissance;
	
	 /*Premier constructeur*/
	public Marin(String nom2,String prenom2,int naissance) {
	    System.out.println("Cr�ation de Marin");
		this.nom = nom2;
		this.prenom = prenom2;
		this.naissance = naissance;

	}
	
	/*Deuxieme constructeur*/
	public Marin(String nom2,int naissance) {
		this(nom2," ",naissance); /*Appel du premier constructeur*/
	}
	
	public void setNom(String nom2) {
		nom = nom2;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setPrenom(String prenom2) {
		prenom = prenom2;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	
	
	public int getNaissance() {
		return naissance;
	}

	public void setNaissance(int naissance) {
		this.naissance = naissance;
	}

	/* Avec outils de g�n�ration de code int�gr�s � Eclipse*/ 
	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
	    	return true;
	    if (obj == null)
	    	return false;
	    if (getClass() != obj.getClass())
	    	return false;
	    
	    Marin marin2 = (Marin) obj;
	    	   
	    if (nom == null) {
	    	if (marin2.nom != null)
	    		return false;	
	    } 
	    else if (!nom.equals(marin2.nom))
	    		return false;
	    
	    if (prenom == null) {
	    	if (marin2.prenom != null)
	    		return false;
	    } 
	    else if (!prenom.equals(marin2.prenom))
	    		return false;
	    
	    if (naissance != marin2.naissance)
	        return false;
	    
	    return true;
	  }	
	
	
	public boolean comparer(Marin marin) {
		if(nom.compareTo(marin.nom) == 0) { 
			if(prenom.compareTo(marin.prenom) == 0) {
				System.out.println("Noms et prenoms identiques entre les 2 marins");
				return true;
			}
			else System.out.println("Noms identiques entre les 2 marins mais pas les prenoms");
		}
		else System.out.println("Noms pas identiques entre les 2 marins");
		
		return false;
	}
}
