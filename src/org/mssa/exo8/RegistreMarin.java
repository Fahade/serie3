package org.mssa.exo8;
import java.util.*;

public class RegistreMarin {
	
    
 // cr�ation d'une table dont les cl�s sont des String
    // et les valeurs des marins
   private Map<Integer, HashSet <Marin>> map =  new HashMap<Integer, HashSet <Marin>>();
   HashSet<Marin> hs1950 = new HashSet<Marin>();
   HashSet<Marin> hs1960 = new HashSet<Marin>();
   HashSet<Marin> hs1970 = new HashSet<Marin>();
   HashSet<Marin> hs1980 = new HashSet<Marin>();  
   
   public void addMarin(Marin marin) {
	   if (marin.getNaissance() >= 1950 && marin.getNaissance() <1960) {
		   hs1950.add(marin);    	 
		   map.put(1950, hs1950);
	   }	
	   else if (marin.getNaissance() >= 1960 && marin.getNaissance() < 1970) {
		   hs1960.add(marin);    	 
		   map.put(1960, hs1960);
	   }
	   else if (marin.getNaissance() >= 1970 && marin.getNaissance() < 1980) {
		   hs1970.add(marin);    	 
		   map.put(1970, hs1970);
	   }
	   else {
		   hs1980.add(marin);    	 
		   map.put(1980, hs1980);	   
	   }
   }
   
   public HashSet<Marin> get(int decennie) {
	   return map.get(decennie);
   }
   
   public int count(int decennie) {
	   	   
	   return map.get(decennie).size();
   }
   
   public int  countT() {
	   return hs1950.size()+hs1960.size()+hs1970.size()+hs1980.size();
   }
   
   public String toString() {  
	    return " [Table de hachage : "+map+"] ";  
   } 
}
