package org.mssa.exo7;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int nombreMarins = 5;
		Equipage equipage =  new Equipage (5) ;  // cr�ation du tableau  
	    
		Marin [] marins =  new Marin [nombreMarins] ;  // cr�ation du tableau 
		marins[0] =  new Marin("Rose","Derrick",1200) ;  // initialisation de chaque case du tableau  
	    marins[1] =  new Marin("Jordan","Micheal",2500) ;  // initialisation de chaque case du tableau  
	    marins[2] =  new Marin("James","Lebron",2300) ;  // initialisation de chaque case du tableau  
	    marins[3] =  new Marin("Curry","Steph",2000) ;  // initialisation de chaque case du tableau  
	    marins[4] =  new Marin("Johnson","Magic",1800) ;  // initialisation de chaque case du tableau  
	    
	    Marin m = new Marin("Doe","John",1600) ;
    	for(Marin marin : marins) {
    		equipage.addMarin(marin);
    	}

	    equipage.addMarin(m);
	    System.out.println("Marin pr�sent dans l'�quipage : " + equipage.isMarinPresent(m));
	    System.out.println("Nombre marins : " + equipage.getNombreMarins(equipage));                                                      
		System.out.println("Moyenne salaire : " + equipage.getMoyenneSalaire(equipage)); 
		
	    equipage.removeMarin(m);
	    System.out.println("Marin pr�sent dans l'�quipage : " + equipage.isMarinPresent(m));
		System.out.println("Nombre marins : " + equipage.getNombreMarins(equipage));                                                      
		System.out.println("Moyenne salaire : " + equipage.getMoyenneSalaire(equipage));                                                      

		Equipage equipage2 = new Equipage (4);
		equipage2.addAllEquipage(equipage);
	    System.out.println("Nombre marins : " + equipage2.getNombreMarins(equipage2));                                                      

	}

}
