package org.mssa.exo7;
import java.util.*;

public class Equipage {
	
	private int nombreMax;
	
	public Equipage(int nombreMax) {
		this.nombreMax = nombreMax;
	}
    HashSet<Marin> hs = new HashSet<Marin>();

    
    /* Sans nombreMax
    public boolean addMarin(Marin marin) {
    	return hs.add(marin);    	
    }*/
           
    public boolean addMarin(Marin marin) {
    	if(hs.size()>=this.nombreMax) {
    		System.out.println("Impossible d'ajouter un marin, nombre de marins maximum atteint ("+this.nombreMax+")");
    		return false;
    	}	
    	return hs.add(marin);    	
    }
    
    public boolean removeMarin(Marin marin) {
    	return hs.remove(marin);
    }
    
    public boolean isMarinPresent(Marin marin) {
    	return hs.contains(marin);
    }
    
   // @Override
    public String toString() {  
	    return "Equipage [equipage : "+hs+"]";  
    }   
    
    public void addAllEquipage(Equipage equipage) {
    	for(Marin marin : equipage.hs) {
    		this.addMarin(marin); 
    	}
    }
    
    public void clear(Equipage equipage) {
    	for(Marin marin : equipage.hs)
    		this.removeMarin(marin); 
    }
    
    public int getNombreMarins(Equipage equipage) {
    	int nb = 0;
    	for(int i = 0;i<equipage.hs.size();i++)
    		nb++; 
    	return nb;
    }
    
    public double getMoyenneSalaire(Equipage equipage) {
    	int sum = 0;
    	for(Marin marin : equipage.hs)
    		sum = sum + marin.getSalaire(); 
    	return (double)sum/getNombreMarins(equipage);
    }
    
}
