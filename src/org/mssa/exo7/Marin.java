package org.mssa.exo7;

public class Marin {
	private String nom;
	private String prenom;
	private int salaire;
	
	 /*Premier constructeur*/
	public Marin(String nom2,String prenom2,int salaire2) {
	    System.out.println("Cr�ation de Marin");
		nom = nom2;
		prenom = prenom2;
		salaire = salaire2;

	}
	
	/*Deuxieme constructeur*/
	public Marin(String nom2,int salaire2) {
		this(nom2," ",salaire2); /*Appel du premier constructeur*/
	}
	
	public void setNom(String nom2) {
		nom = nom2;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setPrenom(String prenom2) {
		prenom = prenom2;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public void setSalaire(int salaire2) {
		salaire = salaire2;
	}
	
	public int getSalaire() {
		return salaire;
	}
	
	public void augmenteSalaire(int augmentation) {
		salaire = salaire + augmentation; 
	}
	
	public String toString() {  
	    return "Marin [nom " + nom + "prenom " + prenom + "salaire " + salaire+"]";  
	}
	 	
	/* Avec outils de g�n�ration de code int�gr�s � Eclipse*/ 
	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
	    	return true;
	    if (obj == null)
	    	return false;
	    if (getClass() != obj.getClass())
	    	return false;
	    
	    Marin marin2 = (Marin) obj;
	    	   
	    if (nom == null) {
	    	if (marin2.nom != null)
	    		return false;	
	    } 
	    else if (!nom.equals(marin2.nom))
	    		return false;
	    
	    if (prenom == null) {
	    	if (marin2.prenom != null)
	    		return false;
	    } 
	    else if (!prenom.equals(marin2.prenom))
	    		return false;
	    
	    if (salaire != marin2.salaire)
	        return false;
	    
	    return true;
	  }	
	
	
	public boolean comparer(Marin marin) {
		if(nom.compareTo(marin.nom) == 0) { 
			if(prenom.compareTo(marin.prenom) == 0) {
				System.out.println("Noms et prenoms identiques entre les 2 marins");
				return true;
			}
			else System.out.println("Noms identiques entre les 2 marins mais pas les prenoms");
		}
		else System.out.println("Noms pas identiques entre les 2 marins");
		
		return false;
	}
}
